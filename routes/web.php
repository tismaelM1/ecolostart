<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ImportController;
use App\Http\Controllers\CalculAutoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('welcome', function () {
    return view('welcome');
})->name('home');



Route::get('/classe/{class}', [ImportController::class, 'perClass'])->name('perclass');


Route::get('/digital', function () {
    return view('digital');
})->name('digitalScreen');

Route::get('/', function () {
    return view('welcome');
})->name('diagram');

// Route::get('/formulaire', function () {
//     return view('formulaire.index');
// })->name('formulaire');

Route::get('redirect_form/{digital_id}', [ImportController::class, 'redirectForm'])->name('redirectForm');

Route::get('/file-import', [ImportController::class, 'index'])->name('import.index');

Route::post('/file-import', [ImportController::class, 'fileImport'])->name('file-import');

Route::get('/auto_calculate', [CalculAutoController::class, 'index'])->name('auto_calcul');


// Route::get('/formulaire/{id}', function () {
//     return view('formulaire.index');
// })->name('formulaire');




// Route::get('/formulaire', [ImportController::class, 'index'])->name('formulaire');


