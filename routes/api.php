<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::get('total_student_co2', [ApiController::class, 'totalStudentCo2'])->name('totalStudentCo2');
Route::post('total_student_per_date_co2', [ApiController::class, 'totalPerDateCo2'])->name('totalPerDateCo2');

Route::get('total_all_student_alternance', [ApiController::class, 'totalAllStudentAlternance'])->name('totalAllStudentAlternance');
Route::get('total_all_student_ecole', [ApiController::class, 'totalAllStudentEcole'])->name('totalAllStudentEcole');


Route::get('get_score_class_travail', [ApiController::class, 'getScoreClassTravail'])->name('getScoreClassTravail');
Route::post('get_score_class_travail_per_date', [ApiController::class, 'getScoreClassTravailPerDate'])->name('getScoreClassTravailPerDate');

Route::get('get_scoreclass_ecole', [ApiController::class, 'getScoreClassEcole'])->name('getScoreClassEcole');
Route::post('get_scoreclass_ecole_per_date', [ApiController::class, 'getScoreClassEcolePerDate'])->name('getScoreClassEcolePerDate');

Route::get('get_score_transport', [ApiController::class, 'getScoreTransport'])->name('getScoreTransport');


Route::post('store_form_api', [ApiController::class, 'storeFormApi'])->name('storeFormApi');





