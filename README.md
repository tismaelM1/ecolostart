<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
<h1>Lancement du projet ecolo</h1>

____version et techno____
    Laravel 8 / php8.0.9   / mysql 8.0.21 

1 : Création base de données avec le nom dans l'env : "laravel" 

2 : Clonage du projet :
- npm install
- composer install 

3 : Démarrer le projet : php artisan serve

4 : remplissage BDD
- Étape 1 : php artisan migrate // php artisan migrate:fresh ==> si déja existant
- Étape 2 : php artisan db:seed
- Étape 3 : aller URL suivante : http://localhost:8000/file-import
- Étape 4 : telecharger les fichier au wetransfert suivant : https://we.tl/t-UgQuYqboRy
- Étape 5 : importer les 2 fichiers Csv étudiants et entreprise dans l'odre (premier : etudiantcsv/ second entreprise.csv)
- Étape 5 : php artisan db:seed associateTransports
- le tour est jouer vous pouvez si vous le souhaitez lancer la tache cron get quelque temps pour qu'elle génère automatiquement des calcul de co2 pour vous ; attention cela peut prendre plusieurs minutes. 





<a href="https://ibb.co/ys6vvLn"><img src="https://i.ibb.co/w6SGGDN/image-1.png" alt="image-1" border="0"></a>
<br>
0    0    1    *    *    /usr/local/bin/php /home/uftuarxy/https://ecolo.tismatek.com/ecolo/artisan schedule:run >> /dev/null 2>&1<br>
La commande à  inserer dans la configuration serveur tache crons<br>
<a href="https://imgbb.com/"><img src="https://i.ibb.co/1d87yfR/image-2.png" alt="image-2" border="0"></a><br>

protected function schedule(Schedule $schedule)<br>
    {<br>
        $schedule->call(function () {<br>
            $this->index();<br>
        })->monthly();<br>
        // Exécutez la tâche le premier jour de chaque mois à 00h00<br>

    }

Les commande ci-dessus font reference a la configuration de la tache cron crée sur un serveu mutulalise cpanel . IL sert d'exemple mais peut differer selon les serveur. 
La tache cron de mise a jour des calcul peut fonctionner également en get mais attantion a configurer vos timeout et time execution sur ^votre configuration de PHP



Route de l'API : <br>
<a href="https://ibb.co/myzkX2r"><img src="https://i.ibb.co/hd8bZ4x/img3.png" alt="img3" border="0"></a>