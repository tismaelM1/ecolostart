<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // generate transport
        DB::table('info_transports')->insert([
            'name' => 'velo',
            'consomate' => (0),
            // 'order' => random_int(1,3),

        ]);
        DB::table('info_transports')->insert([
            'name' => 'marche',
            'consomate' => (0),
            // 'order' => random_int(1,3),

        ]);
        DB::table('info_transports')->insert([
            'name' => 'trottinette',
            'consomate' =>(2),
            // 'order' => random_int(1,3),

        ]);
        DB::table('info_transports')->insert([
            'name' => 'metro',
            'consomate' =>(2.5),
            // 'order' => random_int(1,3),

        ]);
        DB::table('info_transports')->insert([
            'name' => 'ecar',
            'consomate' => (19.8),
            // 'order' => random_int(1,3),

        ]);
        DB::table('info_transports')->insert([
            'name' => 'scooter',
            'consomate' =>(61.60),
            // 'order' => random_int(1,3),

        ]);
        DB::table('info_transports')->insert([
            'name' => 'bus',
            'consomate' =>(103),
            // 'order' => random_int(1,3),

        ]);
        DB::table('info_transports')->insert([
            'name' => 'car',
            'consomate' => (193),
            // 'order' => random_int(1,3),

        ]);
        DB::table('info_transports')->insert([
            'name' => 'RER et transilien',
            'consomate' =>(4.1),
            // 'order' => random_int(1,3),

        ]);
        DB::table('info_transports')->insert([
            'name' => 'TER',
            'consomate' => (24.8),
            // 'order' => random_int(1,3),

        ]);
        DB::table('info_transports')->insert([
            'name' => 'TGV',
            'consomate' => (1.73),
            // 'order' => random_int(1,3),

        ]);
        DB::table('info_transports')->insert([
            'name' => 'Tramway',
            'consomate' =>(2.2),
            // 'order' => random_int(1,3),

        ]);
        DB::table('info_transports')->insert([
            'name' => 'Intercite',
            'consomate' => (5.9),
            // 'order' => random_int(1,3),

        ]);
        DB::table('info_transports')->insert([
            'name' => 'Bus electrique',
            'consomate' => (9.5),
            // 'order' => random_int(1,3),

        ]);

        //  FIN DE generate transport

        // generate transport
        DB::table('classes')->insert([
            'id_class_digital' => random_int(999999999,9999999999999999),
            'name' => 'M1DMM',
            'jour_ecole' => (5),
            'jour_travail' => (15),
        ]);
        DB::table('classes')->insert([
            'id_class_digital' => random_int(999999999,9999999999999999),
            'name' => 'M2DMM',
            'jour_ecole' =>(5),
            'jour_travail' =>(15),
        ]);
        DB::table('classes')->insert([
            'id_class_digital' => random_int(999999999,9999999999999999),
            'name' => 'M1TL',
            'jour_ecole' =>(5),
            'jour_travail' =>(15),
        ]);
        DB::table('classes')->insert([
            'id_class_digital' => random_int(999999999,9999999999999999),
            'name' => 'M2TL',
            'jour_ecole' =>(5),
            'jour_travail' =>(15),
        ]);
        DB::table('classes')->insert([
            'id_class_digital' => random_int(999999999,9999999999999999),
            'name' => 'M1DAA',
            'jour_ecole' =>(5),
            'jour_travail' =>(15),
        ]);
        DB::table('classes')->insert([
            'id_class_digital' => random_int(999999999,9999999999999999),
            'name' => 'M2DAA',
            'jour_ecole' =>(5),
            'jour_travail' =>(15),
        ]);
        DB::table('classes')->insert([
            'id_class_digital' => random_int(999999999,9999999999999999),
            'name' => 'PM2',
            'jour_ecole' =>(5),
            'jour_travail' => (15),
        ]);
        DB::table('classes')->insert([
            'id_class_digital' => random_int(999999999,9999999999999999),
            'name' => 'M1DMBC',
            'jour_ecole' =>(5),
            'jour_travail' =>(15),
        ]);
        DB::table('classes')->insert([
            'id_class_digital' => random_int(999999999,9999999999999999),
            'name' => 'M1UX',
            'jour_ecole' =>(5),
            'jour_travail' =>(15),
        ]);
        DB::table('classes')->insert([
            'id_class_digital' => random_int(999999999,9999999999999999),
            'name' => 'M2UX',
            'jour_ecole' => (5),
            'jour_travail' => (15),
        ]);
        DB::table('classes')->insert([
            'id_class_digital' => random_int(999999999,9999999999999999),
            'name' => 'B1B',
            'jour_ecole' => (20),
            'jour_travail' => (0),
        ]);
        DB::table('classes')->insert([
            'id_class_digital' => random_int(999999999,9999999999999999),
            'name' => 'B1C',
            'jour_ecole' => (20),
            'jour_travail' => (0),
        ]);

            // fin de classes




        for ($i=0; $i <5 ; $i++) { 
            DB::table('etudiant')->insert([
                'id_digital' => random_int(999999999,9999999999999999),
                'class_id' => random_int(1,12),
            ]);
        }



    }
}