<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class associateTransports extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i <200 ; $i++) { 
            DB::table('associate_transport')->insert([
                'etudiant_id' => random_int(1,400),
                'transport_id' => random_int(1,14),
                'type' => 'ecole',
                'percent' => random_int(10,100),

            ]);
        }
        for ($i=0; $i <200 ; $i++) { 
            DB::table('associate_transport')->insert([
                'etudiant_id' => random_int(1,400),
                'transport_id' => random_int(1,14),
                'type' => 'pro',
                'percent' => random_int(10,100),
            ]);
        }
    }
}
