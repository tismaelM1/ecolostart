<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AssociateInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associate_info', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('etudiant_id');
            $table->foreign('etudiant_id')
                  ->references('id')
                  ->on('etudiant')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('info_id');
            $table->foreign('info_id')
                ->references('id')
                ->on('info_etudiants')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associate_info');
    }
}
