<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AssociateTransport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associate_transport', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('etudiant_id');
            $table->foreign('etudiant_id')
                  ->references('id')
                  ->on('etudiant')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('transport_id');
            $table->foreign('transport_id')
                ->references('id')
                ->on('info_transports')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->string('type');
            $table->integer('percent');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associate_transport');

    }
}
