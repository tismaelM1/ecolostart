<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_class_digital');
            $table->string('name',25);
            $table->integer('jour_ecole');
            $table->integer('jour_travail');
            // $table->timestamps();
        });

        Schema::table('etudiant', function (Blueprint $table) {
            $table->foreign('class_id')
            ->references('id')
            ->on('classes')
            ->onDelete('cascade')->onUpdate('cascade');
            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classes');
    }
}
