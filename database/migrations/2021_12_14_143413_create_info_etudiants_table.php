<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfoEtudiantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_etudiants', function (Blueprint $table) {
            $table->id();
            $table->string('address_pers');
            $table->string('address_pro')->nullable();
            // $table->float('co2_ecole')->default(0);
            // $table->float('co2_travail')->default(0);
            $table->bigInteger('co2_ecole')->default(0);
            $table->bigInteger('co2_travail')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_etudiants');
    }
}
