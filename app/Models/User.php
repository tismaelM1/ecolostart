<?php

namespace App\Models;

use App\Models\InfoEtudiant;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use  HasFactory;

    protected $table = "etudiant";

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id_digital',
        'class_id',
    ];


    public function infoEtudiants()
    {
        // return $this->belongsToMany(InfoEtudiant::class);
        return $this->belongsToMany(InfoEtudiant::class,'associate_info', 'etudiant_id', 'info_id');
        // , 'role_user', 'user_id', 'role_id'
    }
    // public function categories(){
    //     return $this->belongsToMany('App\Models\infoEtudiants');
    // }

    public function infoTransports()
    {
        return $this->belongsToMany(InfoTransport::class,'associate_transport', 'etudiant_id', 'transport_id')->withPivot('percent', 'type');
        // , 'role_user', 'user_id', 'role_id'
    }





}
