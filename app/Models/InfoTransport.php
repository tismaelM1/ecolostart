<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class InfoTransport extends Model
{
    use HasFactory;

    protected $table = "info_transports";

    public $timestamps = true;

    protected $fillable = [
        'etudiant_id',
        'transport_id',
        'type',
        "percent"
    ];
}
