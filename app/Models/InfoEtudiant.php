<?php

namespace App\Models;

// use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class InfoEtudiant extends Model
{
    protected $table= "info_etudiants";
    use HasFactory;

    public $timestamps = true;

    protected $fillable = [
        'address_pers',
        'address_pro',
        'co2_ecole',
        'co2_travail',
    ];


    public function users()
    {
        return $this->belongsToMany('App\Models\User','associate_info','info_id','etudiant_id');
        // , 'role_user', 'user_id', 'role_id'
    }
    // return $this->belongsToMany('App\Models\infoEtudiant','associate_info', 'etudiant_id', 'info_id');



}
