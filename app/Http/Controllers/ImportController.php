<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\InfoEtudiant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class ImportController extends Controller
{
    
    public function perClass(Request $request){

        // dd(phpinfo());
        //  $datechoose = Carbon::now()->subMonth(3);
         $months = array(1 => 'Jan.', 2 => 'Feb.', 3 => 'Mar.', 4 => 'Apr.', 5 => 'May', 6 => 'Jun.', 7 => 'Jul.', 8 => 'Aug.', 9 => 'Sep.', 10 => 'Oct.', 11 => 'Nov.', 12 => 'Dec.');

         
        // public $date2 = Carbon::now();
        
        $class = $request->class;
        $classFind = DB::table('classes')->where('name',$class)->first();
        if ($classFind !=null) {

            $resultQueryTravail = DB::table('info_etudiants')->select(DB::raw( "name, SUM(jour_travail * co2_travail) as sumresult"))
            ->join('associate_info', 'info_etudiants.id', '=', 'associate_info.info_id')
            ->join('etudiant', 'etudiant.id', '=', 'associate_info.etudiant_id')
            ->join('classes', 'classes.id', '=', 'etudiant.class_id')->where("name",$classFind->name)->groupBy('name')->first()
            // ->sum(DB::raw((int)'jour_travail'*(float)'co2_travail'))
            ;
            $resultQueryEcole = DB::table('info_etudiants')->select(DB::raw( "name, SUM(jour_ecole * co2_ecole) as sumresult"))
                ->join('associate_info', 'info_etudiants.id', '=', 'associate_info.info_id')
                ->join('etudiant', 'etudiant.id', '=', 'associate_info.etudiant_id')
                ->join('classes', 'classes.id', '=', 'etudiant.class_id')->where("name",$classFind->name)->groupBy('name')->first()
                // ->sum(DB::raw((int)'jour_travail'*(float)'co2_travail'))
            ;

            $array_name = [];
            $array_result = [];

            for ($i=5; $i >=0 ; $i--) { 
                $dateQuery=date('m')-$i;
                $resultQuery = $this->getQueryScoreClassMonth($classFind->name,$dateQuery);
                // dd($resultQuery);
                if ($resultQuery!=null) {
                    // dd($resultQuery);
                    array_push($array_name,$months[$dateQuery]);
                    array_push($array_result,intval($resultQuery->sumresult));
                }else {
                    // dd($months[$dateQuery]);
                    array_push($array_name,$months[$dateQuery]);
                    array_push($array_result,0);
                }


            }


            // $resultGlob = [
            //     "requette_globale"=>$resultQuery,
            //     "label_for_data_chart"=>$array_name,
            //     "sum_for_data_chart"=>$array_result
            // ];

            // dd($resultGlob);


            return view('class')
                ->with("sumEcole",$resultQueryEcole->sumresult)
                ->with("sumTravail",$resultQueryTravail->sumresult)
                ->with('labelChart',json_encode($array_name))
                ->with('dataChart',json_encode($array_result))
                ->with('className',$classFind->name)


            ;


            
        }
    }


    public function getQueryScoreClassMonth ($class,$month){
        $resultQuery = DB::table('info_etudiants')->select(DB::raw( "name, SUM(jour_ecole * co2_ecole) as sumresult"))
        ->join('associate_info', 'info_etudiants.id', '=', 'associate_info.info_id')
        ->join('etudiant', 'etudiant.id', '=', 'associate_info.etudiant_id')
        ->join('classes', 'classes.id', '=', 'etudiant.class_id')->where('name',$class)->whereMonth('info_etudiants.created_at',
            $month
        )->groupBy('name')->first();

        // dd($resultQuery);

        return $resultQuery;
    }

    public function fileImport(Request $request){

        $csv_etudiant = $request->file("etudiant");
        $csv_entreprise = $request->file("entreprise");

    //_____________ pour le fichier exel etudiant 
        $file_etudiant= fopen($csv_etudiant->getPathname(), "r");

        $nameArrayFirst = explode(";",fgetcsv($file_etudiant)[0]);
        // $nameArrayFirst = fgetcsv($file_etudiant)[0];
        // 0 => "saison"
        // 1 => "etat"
        // 2 => "Adresse"
        // 3 => "code_postal"
        // 4 => "ville"
        // 5 => "eleve_id"
        // 6 => "class_id"

        while (($line = fgetcsv($file_etudiant)) !== FALSE) {
            $line = explode(";", $line[0]);

            if ($this->is_array_key($nameArrayFirst,$line)) {
                $user = User::where("id_digital", $line[5])->first();

                // si null alors crer sinon var user deja recup juste en haut
                if (!$this->is_array_empty($line)) {
                    if (!$user) {
                        $user = User::create(array(
                            "id_digital" =>$line[5],
                            "class_id"=>random_int(1,12)
                        ));
                    }
                    $addresse_pers = $line[2]." ".$line[3]." ".$line[4];

                    $infoEtudiant = InfoEtudiant::create(array(
                        "address_pers"=>mb_strtoupper($addresse_pers)
                    ));

                    $user->infoEtudiants()->attach($infoEtudiant->id);
                }
            }
            // dd($line);
        }
        fclose($file_etudiant);

    //_____________ FIN DE pour le fichier exel etudiant 


        //_____________ pour le fichier exel entreprise 
        $file_entreprise= fopen($csv_entreprise->getPathname(), "r");

        $nameArraysecond = explode(";",fgetcsv($file_entreprise)[0]);
        // $nameArraysecond = fgetcsv($file_entreprise)[0];
        // 0 => "type"
        // 1 => "name_entreprise"
        // 2 => "adresse_pers"
        // 3 => "code_postal_pers"
        // 4 => "ville_pers"
        // 5 => "adresse_pro"
        // 6 => "code_postal_pro"
        // 7 => "ville_pro"
        // 8 => "eleve_id"
        // 9 => "class_id"
        // dd($nameArraysecond);
        $arrayninks =[]; 

        while (($line = fgetcsv($file_entreprise)) !== FALSE) {
            $i=0;
            // while ($i <50) {

            $line = fgetcsv($file_entreprise);
            if ($line !=null && !empty($line) && gettype($line)!=='boolean') {
                    $line = explode(";", $line[0]);
            // dd($line);

            if (!$this->is_array_empty($line) && count($line)==9 ) {
                array_push($arrayninks, $line[7]);

                if (!empty($line[8])) {
                    $user2 = User::where("id_digital",$line[8])->first();
                    // $user = User::all();
    
                    // si null alors crer sinon var user deja recup juste en haut
                    if (!$user2) {
                        $user2 = User::create(array(
                            "id_digital" => $line[8],
                            "class_id"=>random_int(1,12)
                        ));
                    }
                    // si le tableau contien aucune info vide
                    $addresse_pers = $line[2]." ".$line[3]." ".$line[4];
                    $addresse_pro = $line[2]." ".$line[3]." ".$line[4];

                    try { 
                        $infoEtudiant = InfoEtudiant::create(array(
                            // "address_pers"=>strval($addresse_pers),
                            "address_pers"=>mb_strtoupper($addresse_pers),

                        // "address_pers"=>"fffff",
                        "address_pro"=>mb_strtoupper($addresse_pro)
                    ));
                          // Closures include ->first(), ->get(), ->pluck(), etc.
                      } catch(\Illuminate\Database\QueryException $ex){ 
                        dd($ex->getMessage()); 
                        // Note any method of class PDOException can be called on $ex.
                      }


                        $user2->infoEtudiants()->attach($infoEtudiant->id);
    
    
                        // dd($line);
                    
                }



            }

            }

            // dd($line);
            $i++;
        }
        fclose($file_entreprise);

        // dd($arrayninks);


    //_____________ FIN DE pour le fichier exel entreprise 

        
        // dd("okayy manyy finish ");
        return "import realiser avec succès";
    }


    public function is_array_empty($array){
        foreach ($array as $value) {
            if (empty($value)) {
                return true;
            }
        }
        return false;
    }

    public function is_array_key($array_main,$array_line){
        foreach ($array_main as $key => $value) {
            if (!array_key_exists($key,$array_line)) {
                return false;
            }
        }
        return true;
    }


    public function index(){
        // $users = User::find(1)->infoEtudiants()->get();
        // $users2 = User::find(1)->infoTransport()->toSql();
        // $users = DB::table("info_etudiants")
        // ->join("associate_info","info_etudiants.id","=","associate_info.info_id")
        // ->get();
        return view("import.index");
    }


    public function redirectForm(Request $request){

        $digital_id = $request->digital_id;
        if (!isset($digital_id)) {
            dd('false error retourner a l\'acceuil');
        }
        
        $etudiant = User::where('id_digital',$digital_id)->first(); 
        if ($etudiant!=null) {
            $url = "https://adoring-thompson-c824f9.netlify.app/?digital_id=".$digital_id;
            return Redirect::intended($url);
        }else dd('utilisateur non existant');

        // dd($request->digital_id);

        // return view("import.index");
    }

}
