<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseController extends Controller
{
    public function handleResponse($data, $msg)
    {
    	$res = [
            'success' => true,
            'data'    => $data,
            'message' => $msg,
        ];
        // dd(response()->json($res, 200));

        return response()->json($res, 200);

    }

    public function handleError($error, $code = 404)
    {
    	$res = [
            'success' => false,
            'message' => $error,
        ];
        return response()->json($res, $code);
    }


    
}
