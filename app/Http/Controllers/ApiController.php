<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\InfoEtudiant;
use Illuminate\Http\Request;
use App\Models\InfoTransport;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\BaseController;

class ApiController extends BaseController
{

    public $date1="2021-12-16";
    public $date2="2021-12-17";


    public function totalStudentCo2(){
        $resultQuery = DB::table('info_etudiants')->sum(DB::raw('co2_ecole + co2_travail'));

        return $this->handleResponse(
            $resultQuery,
           'success requette'
       );
    }
    /**
     * Date en parametre
    */

    public function totalPerDateCo2(Request $request){
        $date1=$request->date1;
        $date2=$request->date2;
        // dd($date1);
        // recupere la somme du CO2 total de tout les eleves entre deux dates (PHP)
        $resultQuery = DB::table('info_etudiants')->whereBetween('created_at',[$date1,$date2])->sum(DB::raw('co2_ecole + co2_travail'));
        return $this->handleResponse(
            $resultQuery,
           'success requette'
       );
    }

    // Somme total de tout les elevee en alternance
    public function totalAllStudentAlternance(){

        $resultQuery = DB::table('info_etudiants')->select(DB::raw( "SUM(jour_travail * co2_travail) as sumresult"))
            ->join('associate_info', 'info_etudiants.id', '=', 'associate_info.info_id')
            ->join('etudiant', 'etudiant.id', '=', 'associate_info.etudiant_id')
            ->join('classes', 'classes.id', '=', 'etudiant.class_id')->get()
            // ->sum(DB::raw((int)'jour_travail'*(float)'co2_travail'))
        ;
        // dd($resultQuery);
        return $this->handleResponse(
            $resultQuery,
           'l\'utilisateur a était crer avec succès'
       );
    }

       // Somme total de tout les elevee en ecole
       public function totalAllStudentEcole(){
        $resultQuery = DB::table('info_etudiants')->select(DB::raw( "SUM(jour_ecole * co2_ecole) as sumresult"))
            ->join('associate_info', 'info_etudiants.id', '=', 'associate_info.info_id')
            ->join('etudiant', 'etudiant.id', '=', 'associate_info.etudiant_id')
            ->join('classes', 'classes.id', '=', 'etudiant.class_id')->get()
            // ->sum(DB::raw((int)'jour_travail'*(float)'co2_travail'))
        ;
        // dd($resultQuery);
        return $this->handleResponse(
            $resultQuery,
           'l\'utilisateur a était crer avec succès'
       );
    }   


// recupere les CO2 total d'un classe les jour ou ils vont à travail 
// recup total co2 avec par classe   travail 
    public function getScoreClassTravail(){

        $resultQuery = DB::table('info_etudiants')->select(DB::raw( "name, SUM(jour_travail * co2_travail) as sumresult"))
            ->join('associate_info', 'info_etudiants.id', '=', 'associate_info.info_id')
            ->join('etudiant', 'etudiant.id', '=', 'associate_info.etudiant_id')
            ->join('classes', 'classes.id', '=', 'etudiant.class_id')->groupBy('name')->get()
            // ->sum(DB::raw((int)'jour_travail'*(float)'co2_travail'))
        ;
        // dd($resultQuery);
        // return $query;

        return $this->handleResponse(
            $resultQuery,
           'l\'utilisateur a était crer avec succès'
       );
    }

    // recupere les CO2 total d'un classe les jour ou ils vont à ecole 
    // recup total co2 avec par classe   ecole 
    public function getScoreClassEcole(){

        $resultQuery = DB::table('info_etudiants')->select(DB::raw( "name, SUM(jour_ecole * co2_ecole) as sumresult"))
            ->join('associate_info', 'info_etudiants.id', '=', 'associate_info.info_id')
            ->join('etudiant', 'etudiant.id', '=', 'associate_info.etudiant_id')
            ->join('classes', 'classes.id', '=', 'etudiant.class_id')->groupBy('name')->get()
            // ->sum(DB::raw((int)'jour_travail'*(float)'co2_travail'))
        ;

        $array_name = [];
        $array_result = [];
        foreach ($resultQuery as $key => $classe) {
            array_push($array_name,$classe->name);
            array_push($array_result,$classe->sumresult);

        }
        $resultGlob = [
            "requette_globale"=>$resultQuery,
            "label_for_data_chart"=>$array_name,
            "sum_for_data_chart"=>$array_result
        ];
        // dd($okayy);


        // dd($resultQuery);
        // return $query;

        return $this->handleResponse(
            $resultGlob,
        'l\'utilisateur a était crer avec succès'
    );
    }


// recupere les CO2 total d'un classe les jour ou ils vont à lécole avec un date en paramètre
// recup moyenne co2 avec ecole  where class, par date between travail 
    public function getScoreClassTravailPerDate(Request $request){

        $resultQuery = DB::table('info_etudiants')->select(DB::raw( "name, SUM(jour_travail * co2_travail) as sumresult"))
            ->join('associate_info', 'info_etudiants.id', '=', 'associate_info.info_id')
            ->join('etudiant', 'etudiant.id', '=', 'associate_info.etudiant_id')
            ->join('classes', 'classes.id', '=', 'etudiant.class_id')->whereBetween('info_etudiants.created_at',[
                $this->date1,
                $this->date2
            ])->groupBy('name')->get()
            // ->sum(DB::raw((int)'jour_travail'*(float)'co2_travail'))
        ;
        // dd($resultQuery);
        // return $query;

        $array_name = [];
        $array_result = [];
        foreach ($resultQuery as $key => $value) {
            array_push($array_name,$value->name);
            array_push($array_result,$value->sumresult);
        }
        $resultGlob = [
            "requette_globale"=>$resultQuery,
            "label_for_data_chart"=>$array_name,
            "sum_for_data_chart"=>$array_result
        ];



        return $this->handleResponse(
            $resultGlob,
           'l\'utilisateur a était crer avec succès'
       );
    }


    public function getScoreClassEcolePerDate(Request $request){
        
        $resultQuery = DB::table('info_etudiants')->select(DB::raw( "name, SUM(jour_ecole * co2_ecole) as sumresult"))
            ->join('associate_info', 'info_etudiants.id', '=', 'associate_info.info_id')
            ->join('etudiant', 'etudiant.id', '=', 'associate_info.etudiant_id')
            ->join('classes', 'classes.id', '=', 'etudiant.class_id')->whereBetween('info_etudiants.created_at',[
                $this->date1,
                $this->date2
            ])->groupBy('name')->get()
            // ->sum(DB::raw((int)'jour_travail'*(float)'co2_travail'))
        ;

        return $this->handleResponse(
            $resultQuery,
            'l\'utilisateur a était crer avec succès'
        );
    }


    public function getScoreTransport(){

        $resultQuery = DB::table('associate_transport')->select(DB::raw( "info_transports.name, COUNT(transport_id) as sumresult"))
            ->join('info_transports', 'associate_transport.transport_id', '=', 'info_transports.id')
            ->groupBy('info_transports.name')
            ->orderByDesc('sumresult')
            ->get()
            // ->sum(DB::raw((int)'jour_travail'*(float)'co2_travail'))
        ;

        $array_name = [];
        $array_result = [];
        foreach ($resultQuery as $key => $value) {
            array_push($array_name,$value->name);
            array_push($array_result,$value->sumresult);
        }
        $resultGlob = [
            "requette_globale"=>$resultQuery,
            "label_for_data_chart"=>$array_name,
            "sum_for_data_chart"=>$array_result
        ];

        // dd($resultGlob);
        // return $query;


        return $this->handleResponse(
            $resultGlob,
            'l\'utilisateur a était crer avec succès'
        );
    }


    public $addresseDigital ="1 rue de la poire 75012 Paris";

    public function storeFormApi(Request $request){
        $id_digital = $request->id_digital;
        $addresse_pers = $request->adresse_domicile;
        $adresse_pro = $request->adresse_entreprise;
        $transport_ecole = $request->transport_campus;
        $transport_pro = $request->transport_entreprise;
        
        $etudiant = User::where('id_digital',$id_digital)->first();
        $pro = false;
        if ($etudiant !=null) {

            if (isset($transport_pro) || ($transport_pro != null && !empty($transport_pro == null)) ) {
                $pro =true;
                $infoEtudiant = InfoEtudiant::create(array(
                    "address_pers"=>mb_strtoupper($addresse_pers),
                    "address_pro"=>mb_strtoupper($adresse_pro)
                ));
            }else {
                $infoEtudiant = InfoEtudiant::create(array(
                    "addresse_pers"=>mb_strtoupper($address_pers)
                ));    
            }
            $etudiant->infoEtudiants()->attach($infoEtudiant->id);

            // dd(json_decode($transport_ecole));
            foreach (json_decode($transport_ecole) as $key => $transport) {
                $infoTransport = InfoTransport::where('name',$transport->vehicule)->first();
                // dd($infoTransport);
                if ( $infoTransport != null && !empty($infoTransport) ) {
                    $this->insertTransport($etudiant->id,$infoTransport->id,"ecole",$transport->percentage);

                }else $this->handleError('probleme avec le formulaire veuilliez ressayer');
                // dd($transport);
            }

            if ($pro) {
                foreach (json_decode($transport_pro) as $key => $transport) {
                    $infoTransport = InfoTransport::where('name',$transport->vehicule)->first();
                    if ( $infoTransport != null && !empty($infoTransport) ) {
                        $this->insertTransport($etudiant->id,$infoTransport->id,"ecole",$transport->percentage);
                    }else $this->handleError('probleme avec le formulaire veuilliez ressayer');
                }
            }
        }
        else {
            return $this->handleError('probleme avec le formulaire veuilliez ressayer');
        }


        // dd($etudiant);
 
            return $this->handleResponse("not data return",'nous avons bien receptionner vos donnees ');

        


        // dd($request->all());
    }

    public function insertTransport($etudiantId,$infoTransport,$ecole,$percent){

        // dd('rff');
        DB::table("associate_transport")->insert([
            "etudiant_id"=>$etudiantId,
            "transport_id"=>$infoTransport,
            "type"=>$ecole,
            "percent"=>$percent
        ]);
    }

}
