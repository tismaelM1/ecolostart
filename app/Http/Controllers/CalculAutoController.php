<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CalculAutoController extends Controller
{

    function import (){
        return view("import.index");
    }
     function index()
    {
        dd('fdd');
        $max_transport_par_etudiant =2;
        $etudiants = User::all();
        // $etudiants = User::where("id",6)->get();
        // dd($etudiants);

        $etudiant_complet =[];
        // $response = Http::get('https://dc-distance.herokuapp.com/16%20RUE%20DU%20BOIS%20JOLY%2092000%20NANTERRE/8%20rue%20de%20la%20fontaine%20aux%20roi%2075011%20paris');
        // Récupérer le contenu de la page Web à partir de l'url.
            $source = "3 rue de flandre 92140 clamart";
            $dest = "8 bis Rue de la Fontaine au Roi, 75011 Paris";

            $distance = null;

            // dd($this->getDistance($source,$dest));
 
        foreach ($etudiants as $key => $etudiant) {
            $infoEtudiants = $etudiant->infoEtudiants()->first();
            // $infoEtudiants = $etudiant->infoEtudiants()->where("id",6)->first();

            // $infoTransports = $etudiant->infoTransports()->orderBy('associate_transport.percent', 'asc')->take(4)->get();
            $infoTransportsPers = $etudiant->infoTransports()->where('type','ecole')->take($max_transport_par_etudiant)->get()->toArray();
            $infoTransportsPro = $etudiant->infoTransports()->where('type','pro')->take($max_transport_par_etudiant)->get()->toArray();

            
            
            // dd($infoEtudiants);
            if (($infoEtudiants!=null  )  ) {
                // dd($infoEtudiants);
                
                if (!$infoTransportsPers==null || count($infoTransportsPers)>0 ) {
                    // dd($infoEtudiants->address_pro);
                    
                   $distance =  $this->getDistance($infoEtudiants->address_pers,$dest);

                    // $distance = 277900.5;
                    if ($distance) {
                        $result = $this->traitTransport($infoTransportsPers,$distance);
                        // dd($infoTransportsPro,$infoTransportsPers,'faire calssscul 1 ');
                        $infoEtudiants->update([
                            'co2_ecole' => intval($result)
                        ]);
                    }

                }
                if ($infoEtudiants->address_pro !=null) {
                    if (!$infoTransportsPro==null || count($infoTransportsPro)>0) {
                        $distance =  $this->getDistance($infoEtudiants->address_pro,$dest);
                        if ($distance) {
                            $result = $this->traitTransport($infoTransportsPro,$distance);
                            $infoEtudiants->update([
                                'co2_travail' => intval($result)
                            ]);
                        }
    
                        // dd($infoTransportsPro,$infoTransportsPers,'faire calcul ');
                    }
                }


                
                // dd([$infoEtudiants,$infoTransportsPers,$infoTransportsPro]);
            }
        }
        // dd($etudiants);
    }

    public function traitTransport($transports,$distance)
    {
        $result = 0;
        $transport1 = $transports[0];
        $percent1 = false;
        $type1 = false;
        $vehiculeConso1 = false;


        $transport2 = false;
        $percent2 = false;
        $type2 = false;
        $vehiculeConso2 = false;

        $percent1 = floatval($transport1['pivot']['percent']);
        $type1 = ($transport1['pivot']['type']);
        $vehiculeConso1 = floatval($transport1['consomate']);

        // dd($vehiculeConso1);

        if (isset($transports[1])) $transport2 = $transports[1];
        
        if ($transport2) {
            // dd($transport2);
            $percent2 = floatval($transport2['pivot']['percent']);
            $type2 = ($transport2['pivot']['type']);
            $vehiculeConso2 = floatval($transport2['consomate']);
            $result = 
            ( 
                ($percent1/100)* ($vehiculeConso1) + 
                ($percent2/100)* ($vehiculeConso2) 
            )*$distance
            ;
            // dd($result);
            // dd($percent2,'545',$type2);
        }else {
            // dd('fdff');
            $result = 
            ( 
                ($percent1/100)* ($vehiculeConso1) 
            )*$distance
            ;
        }


        return $result;
    }

    // return distance float 
    public function getDistance($source,$dest)
    {
        $url = "https://dc-distance.herokuapp.com/".urlencode($source)."/".urlencode($dest); 
        // Initialisez une session CURL.
        $ch = curl_init();  
        // dd($url);
        // Récupérer le contenu de la page
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        
        //Saisir l'URL et la transmettre à la variable.
        curl_setopt($ch, CURLOPT_URL, $url); 
        //Désactiver la vérification du certificat puisque waytolearnx utilise HTTPS
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //Exécutez la requête 
        $result = curl_exec($ch); 
        //Afficher le résultat

        // dd($distance);
        // dd($result);

        $result = json_decode($result);

        if (isset($result->distance)) {
            // dd($result);
            return $result->distance; 
        }
        // dd($result);
        return false;

        
    }
}
