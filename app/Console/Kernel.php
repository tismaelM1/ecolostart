<?php

namespace App\Console;

use App\Models\User;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $this->index();
        })->monthly();
        // Exécutez la tâche le premier jour de chaque mois à 00h00

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        set_time_limit(0);
        $max_transport_par_etudiant =2;
        // $etudiants = User::all();
        $etudiants = User::all()->orderBy('id',"desc");

        $etudiant_complet =[];
        $source = "3 rue de flandre 92140 clamart";
        $dest = "8 bis Rue de la Fontaine au Roi, 75011 Paris";

        $distance = null;

 
        foreach ($etudiants as $key => $etudiant) {
            $infoEtudiants = $etudiant->infoEtudiants()->first();

            $infoTransportsPers = $etudiant->infoTransports()->where('type','ecole')->take($max_transport_par_etudiant)->get()->toArray();
            $infoTransportsPro = $etudiant->infoTransports()->where('type','pro')->take($max_transport_par_etudiant)->get()->toArray();

            if (($infoEtudiants!=null  )  ) {                
                if (!$infoTransportsPers==null || count($infoTransportsPers)>0 ) {
                    
                   $distance =  $this->getDistance($infoEtudiants->address_pers,$dest);

                    if ($distance) {
                        if ($distance >=100000) {
                            $result = $this->traitTransport($infoTransportsPers,$distance);
                            $infoEtudiants->update([
                                'co2_ecole' => intval($result)
                            ]);
                        }

                    }
                }
                if ($infoEtudiants->address_pro !=null) {
                    if (!$infoTransportsPro==null || count($infoTransportsPro)>0) {
                        $distance =  $this->getDistance($infoEtudiants->address_pro,$dest);
                        if ($distance) {
                            if ($distance >=100000) {
                                $result = $this->traitTransport($infoTransportsPro,$distance);
                                $infoEtudiants->update([
                                    'co2_travail' => intval($result)
                                ]);
                            }

                        }
                    }
                }
            }
        }
    }

    public function traitTransport($transports,$distance)
    {
        $result = 0;
        $transport1 = $transports[0];
        $percent1 = false;
        $type1 = false;
        $vehiculeConso1 = false;


        $transport2 = false;
        $percent2 = false;
        $type2 = false;
        $vehiculeConso2 = false;

        $percent1 = floatval($transport1['pivot']['percent']);
        $type1 = ($transport1['pivot']['type']);
        $vehiculeConso1 = floatval($transport1['consomate']);

        // dd($vehiculeConso1);

        if (isset($transports[1])) $transport2 = $transports[1];
        
        if ($transport2) {
            // dd($transport2);
            $percent2 = floatval($transport2['pivot']['percent']);
            $type2 = ($transport2['pivot']['type']);
            $vehiculeConso2 = floatval($transport2['consomate']);
            $result = 
            ( 
                ($percent1/100)* ($vehiculeConso1) + 
                ($percent2/100)* ($vehiculeConso2) 
            )*$distance
            ;
            // dd($result);
            // dd($percent2,'545',$type2);
        }else {
            // dd('fdff');
            $result = 
            ( 
                ($percent1/100)* ($vehiculeConso1) 
            )*$distance
            ;
        }


        return $result;
    }

    // return distance float 
    public function getDistance($source,$dest)
    {
        $url = "https://dc-distance.herokuapp.com/".urlencode($source)."/".urlencode($dest); 
        // Initialisez une session CURL.
        $ch = curl_init();  
        // dd($url);
        // Récupérer le contenu de la page
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        
        //Saisir l'URL et la transmettre à la variable.
        curl_setopt($ch, CURLOPT_URL, $url); 
        //Désactiver la vérification du certificat puisque waytolearnx utilise HTTPS
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //Exécutez la requête 
        $result = curl_exec($ch); 
        //Afficher le résultat

        // dd($distance);
        // dd($result);

        $result = json_decode($result);

        if (isset($result->distance)) {
            // dd($result);
            return $result->distance; 
        }
        // dd($result);
        return false;

        
    }
}
