@extends('layouts.app')
@section('contain')

<div class="container">
    <div class="row mt-5">
      <h1 class="my-5"><a href="/welcome">Acceder a lancien home page</a> </h1>

        <h1 class="my-5">DIGITAL CO2 APP avec requette api reel donnée</h1>
        <div class="col-12 card bg-light p-5">
            <div class="shadow-lg rounded-lg overflow-hidden">
                <div class="py-3 px-12 bg-gray-50">Bar chart</div>
                
                <canvas class="p-10" id="chartBar"></canvas>
            </div>
    
        </div>
        <div class="col-xl-4 col-md-6 col-xs-12 bg-light">
          <canvas class="p-10" id="chartBarY"></canvas>
        </div>

    </div>

  <div class="row">
    <button id="btnChange"> changer </button>

  </div>
</div>

<div class="my-5 py-5"></div>




@endsection

@section('extra-js')
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
      const sendGetRequest = async () => {
          try {
              const resp = await axios.get('http://localhost:8000/api/get_scoreclass_ecole');
              const data = await resp.data
              console.log(data['data']);
              LineChartsX(data['data'])
          } catch (err) {
              // Handle Error Here
              console.error(err);
          }

          try {
              const resp = await axios.get('http://localhost:8000/api/get_score_transport');
              const data = await resp.data
              console.log(data['data']);
              LineChartsY(data['data'])

          } catch (err) {
              // Handle Error Here
              console.error(err);
          }

      };

      sendGetRequest();

      const LineChartsX = (data)=>{
        const labelsBarChart = data.label_for_data_chart;

        const dataBarChart = {
          labels: labelsBarChart,
          datasets: [
            {
              label: "get_scoreclass_ecole",
              backgroundColor: "hsl(252, 82.9%, 67.8%)",
              borderColor: "hsl(252, 82.9%, 67.8%)",
              // data: [0, 10, 5, 2, 20, 30, 45,5,4,4,6,5],
              data: data.sum_for_data_chart,
            }
          ],

        };
      
        const configBarChart = {
          type: "line",
          data: dataBarChart,
          options: { indexAxis: 'x',},
        };
      
        var chartBar = new Chart(
          document.getElementById("chartBar"),
          configBarChart
        );
      }



      const LineChartsY = (data)=>{
        const labelsBarChart = data.label_for_data_chart;

        const dataBarChart = {
          labels: labelsBarChart,
          datasets: [
            {
              label: "get_scoreclass_ecole",
              // backgroundColor: "hsl(252, 82.9%, 67.8%)",
//               borderColor: "hsl(252, 82.9%, 67.8%)",
              // data: [0, 10, 5, 2, 20, 30, 45,5,4,4,6,5],
              backgroundColor: [
                'rgba(205, 99, 132, 0.5)',
                'rgba(54, 162, 45, 0.2)',
                'rgba(41, 124, 102, 0.5)',
                'rgba(54, 162, 0, 0.2)',
                'rgba(124, 99, 192, 0.5)',
                'rgba(54, 0, 235, 0.2)',
                'rgba(255, 99, 124, 0.5)',
                'rgba(54, 40, 235, 0.2)',
              ],
              data: data.sum_for_data_chart,
            }
          ],

        };
      
        const configBarChart = {
          type: "pie",
          data: dataBarChart,
          options: { indexAxis: 'y',},
        };
      
        var chartBar = new Chart(
          document.getElementById("chartBarY"),
          configBarChart
        );
      }



  }); 

  </script>
@endsection