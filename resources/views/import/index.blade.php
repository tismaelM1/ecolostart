@extends('layouts.app')
@section('contain')

<div class="container">
    <div class="row mt-5">
        <h1 class="my-5">DIGITAL CO2 APP -- GENERATION DES imports</h1>

    </div>

    <div class="row">
            {{-- <button class="btn btn-secondary w-50 me-3">generer csv </button> --}}
            <form class="col-12 d-flex justify-content-between" action="{{ route('file-import') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <input type="file"accept=".csv" name="etudiant" id="" value="import etudiant" class="btn btn-secondary w-50 me-3">
                <input type="file"accept=".csv" name="entreprise" id="" value="import entreprise" class="btn btn-secondary w-50 me-3">

                <button class="btn btn-secondary w-50 ms-3" type="submit">soumission du formulaire</button>

            </form>


        <div class="col mt-5">

            <button class="btn btn-secondary w-50 ms-3">mise a jour des calcul manuel</button>

            <a href="{{route('home')}}" class="btn btn-light">retour n arrière</a>
        </div>

    </div>

    <div class="autocomplete-container" id="autocomplete-container"></div>


</div>


@section('extra-js')

@endsection






