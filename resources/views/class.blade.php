@extends('layouts.app')
@section('contain')

<div class="nav navbar-nav">
  <nav class="nav nav-pills nav-justified">
    {{-- <a class="nav-link btn btn-light text-white" href="{{route('redirectForm',['digital_id'=>10022321549725816958])}}">Formulaire</a> --}}
    <a class="nav-link btn-light text-white" href="{{route('import.index')}}">Import</a>
    <a class="nav-link btn-light text-white" href="{{route('diagram')}}">Diagram CO2</a>
  </nav>
</div>


<div class="container">
  <div class="row mt-3">
    <h1 class="text-center">Analyse des émissions de CO2 lié au transport des étudiants de la classe <b class="text-warning">{{$className}}</b> de Digital Campus <b class="text-warning">Paris</b> pour leur scolarité</h1>
    <div class="col-6 card bg-light p-5">
      <div class="shadow-lg rounded-lg overflow-hidden">
        <div class="py-3 px-12 bg-gray-50 text-center text-dark"></div>
        <canvas class="p-10" id="chartBar"></canvas>
      </div>
    </div>
    <div class="col-6">
      <p class="text-md-left text-center">
        Votre classe émet <b class="text-warning"> {{$sumEcole}} kgCO2</b> pour aller jusqu'à Digital Campus et 
        <b class="text-warning"> {{$sumTravail}} kgCO2</b> pour se rendre sur son lieu de travail.
        <br>

      </p>

    </div>
  </div>
  <div class="row my-5">
    <button id="btnChange"> changer </button>

  </div>
</div>

<div class="my-5 py-5"></div>




@endsection

@section('extra-js')
<script>
  document.addEventListener("DOMContentLoaded", function(event) {
 

    const labelsBarChart = {!! $labelChart !!}


const dataBarChart = {
  labels: labelsBarChart,
  datasets: [
    {
      label: "get_scoreclass_ecole",
      backgroundColor: "hsl(252, 82.9%, 67.8%)",
      borderColor: "hsl(252, 82.9%, 67.8%)",
      // data: [0, 10, 5, 2, 20, 30, 45,5,4,4,6,5],
      data: {!! $dataChart !!},
    }
  ],

};

const configBarChart = {
  type: "line",
  data: dataBarChart,
  options: { indexAxis: 'x',},
};

var chartBar = new Chart(
  document.getElementById("chartBar"),
  configBarChart
);

  });
</script>
@endsection