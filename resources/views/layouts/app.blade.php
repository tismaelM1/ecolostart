<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <style>
            body {
                font-family: 'Nunito', sans-serif;
                background-color: hsl(200,47%,49%);
            }
            body {
              font: 16px;
              /* background: #f3f5f6; */
            }
            nav{
              background: hsl(25,83%,61%);
              width: 90%;
              margin: 1% auto;
              border-radius: 20px;
              border: 1px solid hsl(25, 83%, 56%);
              box-shadow: 2px 2px hsl(200, 47%, 44%);
            }
            .col-6{
              margin: 2% auto;
            }
            .chg{
              width: 10%;
              margin: auto;
            }
            #btnChange{
              color: white;
              background: hsl(25,83%,61%);
              border-radius: 10px;
              border: 2px solid hsl(25,83%,61%);
            }
            .autocomplete-container {
              margin-bottom: 20px;
            }

            .input-container {
              display: flex;
              position: relative;
            }

            .input-container input {
              flex: 1;
              outline: none;
              
              border: 1px solid rgba(0, 0, 0, 0.2);
              padding: 10px;
              padding-right: 31px;
              font-size: 16px;
            }

            .autocomplete-items {
              position: absolute;
              border: 1px solid rgba(0, 0, 0, 0.1);
              box-shadow: 0px 2px 10px 2px rgba(0, 0, 0, 0.1);
              border-top: none;
              background-color: #fff;

              z-index: 99;
              top: calc(100% + 2px);
              left: 0;
              right: 0;
            }

            .autocomplete-items div {
              padding: 10px;
              cursor: pointer;
            }

            .autocomplete-items div:hover {
              /*when hovering an item:*/
              background-color: rgba(0, 0, 0, 0.1);
            }

            .autocomplete-items .autocomplete-active {
              /*when navigating through the items using the arrow keys:*/
              background-color: rgba(0, 0, 0, 0.1);
            }

            .clear-button {
              color: rgba(0, 0, 0, 0.4);
              cursor: pointer;
              
              position: absolute;
              right: 5px;
              top: 0;

              height: 100%;
              display: none;
              align-items: center;
            }

            .clear-button.visible {
              display: flex;
            }

            .clear-button:hover {
              color: rgba(0, 0, 0, 0.6);
            }
        </style>
    </head>
    <body class="antialiased text-light">
        @yield('contain')

          <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.24.0/axios.min.js" integrity="sha512-u9akINsQsAkG9xjc1cnGF4zw5TFDwkxuc9vUp5dltDWYCSmyd0meygbvgXrlc/z7/o4a19Fb5V0OUE58J7dcyw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
          <script src="{{asset('js/chart.min.js')}}"></script>

        @yield('extra-js')
    </body>
</html>
