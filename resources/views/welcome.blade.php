@extends('layouts.app')
@section('contain')


<div class="container-fluid  my-5"  style="padding: 0 7%;">
  <div class="row mt-3" >
    <h1 class="text-center">Analyse des émissions de CO2 liée au transport des étudiants de la classe <b class="text-warning">M1TL</b> de Digital Campus <b class="text-warning">Paris</b> pour leur scolarité</h1>
    <div class="col-6 card p-0">
      <div class="">
        <div class="py-3 px-12 bg-gray-50 text-center text-dark fs-1">Évolution des changements de mode de transport</div>
        <canvas class="p-6 shadow-lg rounded-lg overflow-hidden" id="line-chart"></canvas>
      </div>
    </div>
    <div class="col-6">
        <div class="card bg-light p-0">
          <div class="py-3 px-12 bg-gray-50 text-center text-dark fs-1">Consommation par classe </div>
          <canvas class="shadow-lg rounded-lg overflow-hidden" id="chartBar"></canvas>
        </div>
    </div>
  </div>
  <div class="row d-flex justify-content-start d-none">
    <div class="col-6 ">
      <div class="text-light fs-1">Les moyens de transports les plus utilisées</div>
    </div>
    <div class=" col-2 card bg-light ">
      <canvas style=" height:auto" id="chartBarY"></canvas>
    </div>
  </div>
</div>

@endsection

@section('extra-js')
<script>
document.addEventListener("DOMContentLoaded", function(event) {
      const sendGetRequest = async () => {
          try {
              const resp = await axios.get('http://localhost:8000/api/get_scoreclass_ecole');
              //const json = await resp.json();
              console.log(resp);
              //console.log(json);
          }
          catch (err) {
              // Handle Error Here
              console.error(err);
          }
      }
      sendGetRequest()
});



new Chart(document.getElementById("line-chart"), {
  type: 'line',
  data: {
    labels: ['Jav 2021', 'Fév 2021', 'Mars 2021', 'Avril 2021', 'Mai 2021'],
    datasets: [{ 
        data: [0, 87, 50, 25, 100],
        label: "Total émissions",
        borderColor: "#3e95cd",
        fill: false
      }, { 
        data: [25, 75, 0, 100, 50],
        label: "Total émissions de l'année en cours",
        borderColor: "#8e5ea2",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: 'World population per region (in millions)'
    }
  }
});
</script>
<script>
  document.addEventListener("DOMContentLoaded", function(event) {
    const sendGetRequest = async () => {
        try {
            const resp = await axios.get('http://localhost:8000/api/get_scoreclass_ecole');
            const data = await resp.data
            console.log(data['data']);
            LineChartsX(data['data'])
        } catch (err) {
            // Handle Error Here
            console.error(err);
        }

        try {
            const resp = await axios.get('http://localhost:8000/api/get_score_transport');
            const data = await resp.data
            console.log(data['data']);
            LineChartsY(data['data'])

        } catch (err) {
            // Handle Error Here
            console.error(err);
        }

    };

    sendGetRequest();

    const LineChartsX = (data)=>{
      const labelsBarChart = data.label_for_data_chart;

      const dataBarChart = {
        labels: labelsBarChart,
        datasets: [
          {
            label: "score trajet école par classe",
            backgroundColor: "hsl(252, 82.9%, 67.8%)",
            borderColor: "hsl(252, 82.9%, 67.8%)",
            // data: [0, 10, 5, 2, 20, 30, 45,5,4,4,6,5],
            data: data.sum_for_data_chart,
          }
        ],

      };
    
      const configBarChart = {
        type: "line",
        data: dataBarChart,
        options: { indexAxis: 'x',},
      };
    
      var chartBar = new Chart(
        document.getElementById("chartBar"),
        configBarChart
      );
    }



    const LineChartsY = (data)=>{
      const labelsBarChart = data.label_for_data_chart;

      const dataBarChart = {
        labels: labelsBarChart,
        datasets: [
          {
            label: "score trajet école par classe",
            // backgroundColor: "hsl(252, 82.9%, 67.8%)",
//               borderColor: "hsl(252, 82.9%, 67.8%)",
            // data: [0, 10, 5, 2, 20, 30, 45,5,4,4,6,5],
            backgroundColor: [
              'rgba(205, 99, 132, 0.5)',
              'rgba(54, 162, 45, 0.2)',
              'rgba(41, 124, 102, 0.5)',
              'rgba(54, 162, 0, 0.2)',
              'rgba(124, 99, 192, 0.5)',
              'rgba(54, 0, 235, 0.2)',
              'rgba(255, 99, 124, 0.5)',
              'rgba(54, 40, 235, 0.2)',
            ],
            data: data.sum_for_data_chart,
          }
        ],

      };
    
      const configBarChart = {
        type: "pie",
        data: dataBarChart,
        options: { indexAxis: 'y',},
      };
    
      var chartBar = new Chart(
        document.getElementById("chartBarY"),
        configBarChart
      );
    }



}); 

</script>





@endsection